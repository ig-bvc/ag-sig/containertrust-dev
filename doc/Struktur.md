# Struktur des Repos

Die Idee ist, in einem Git Repo die Fulcio Instanzen und die Metadaten dieser Instanzen zu verwalten.

In einer finalen Installation würden nur einige wenige Administratoren Commit Rechte in diesem Repo erhalten.

Jeder, der für die Allgemeinheit eine eigene Instanz von Fulcio anbieten will, Cloned das Verzeichnis, legt eine neue Branch an, erstellt an der korrekten Stelle seine Änderungen und erstellt einen Pull Request.

Auf diese Weise entsteht ein geordneter Katalog der Fulcio Instanzen, der durch die Administratoren des Repos validiert ist.

## Struktur des Repos:

- ./doc --> Dokumentation
- ./spec --> Spezifikation und Beispieldateien
- ./trust-content/ --> Wurzel des Trustverzeichnisses
  - net.pflaeging/ --> Hier würden zum Beispiel die YAML Dateien der Organisation pflaeging.net liegen

