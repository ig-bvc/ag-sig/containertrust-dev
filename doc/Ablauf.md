# Ablauf

## Verwendung des Truststores

Eine Organisation die auf Basis von kurzlebigen Zertifikaten und gesicherten Logs auf Basis von [cosign](https://docs.sigstore.dev)
und eigenen Instanzen von Fulcio arbeiten möchte, braucht einige Metadaten um Assets zu verifizieren.

Eine Beispielinstallation kann in folgendem Repo bezogen werden: <https://gitlab.opencode.de/ig-bvc/ag-sig/fulcio-rekor-rollout>

Die Idee ist nun Folgende:

- man cloned als Organisation dieses Repo: <https://gitlab.opencode.de/ig-bvc/ag-sig/containertrust-dev>
- Jetzt hat man alle Metadaten der beteiligten Organisationen unter `./trust-content/*/*.yaml`
- Organisatorisch sollte man jetzt festlegen, inwieweit man den dort hinterlegten Stores vertraut:
  - 0 - No trust
  - 1 - vielleicht für isolierte Testinstanzen
  - 2 - Für Development Tests
  - ...
  - 9 - ultimativ, kann direkt in Produktion eingesettzt werden

Für die tatsächliche Verwendung gibt es nun unterschiedliche Level:

### Manuelle Verifikation

Mit einem Programm (oder Script) wird die Glaubwürdigkeit des Assets (meist ein OCI Container) überprüft und anschließend manuell deployed oder in ein eigenes Content- oder Containerrepository importiert.

### Automatischer Import

Ein Import-Job übernimmt vollautomatisch den Import auf Basis der oben genannten Trustlevel den Import in ein die eigenen Repositories. Von dort können dann Builds oder Deployments ausgeführt werden,
bei denen zumindest sicher ist, das die importierten Assets von einer vertauenswürdigen Instanz signiert sind.

### Automatische Validierung beim Deployment

Die ideale Variante ist natürlich eine automatische Verifikation des Assets (meist in der ersten Phase wohl ein OCI Container).

Hierzu ist folgendes notwendig:

- Installation von Gatekeeper (mit entsprechenden Rules), Connaisseur oder dem Sigstore Policy Controller im Kubernetes / OpenShift / OKD Cluster
- Generieren und laden der korrekten Trust Konfiguration aus dem Repo und der eigenen TrustLevel Tabelle

Mit dieser Methode kann der höchste Sicherheits- und Automatisationsgrad erreicht werden.

## Einbringen von neuen Fulcio Instanzen

- Clone dieses Repos
- Branch erzeugen:  
  Vorschlag: meine-organisation-neue-fulcio-instanz
- Verzeichnis anlegen: ./trust-content/de.meine-organisation
- erzeugen eines Eintrags (oder mehrere) nach der Spezifikation unter [../spec/example.yaml](../spec/example.yaml)
- git commit; git push
- Pull Request erstellen mit der genauen Beschreibung
- *Administratoren des Repos*:
  - Validierung des Eintrags
  - Überprüfung der Zuständigkeit (darf die/der das überhaupt?)
  - Reject oder Merge
