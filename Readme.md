# Container-Trust Anker für unterschiedliche Fulcio Provider

hier entsteht der Ankerpunkt (oder besser der Verteilpunkt) für die Metadaten mehrerer Fulcio Instanzen. Damit kann jeder Teilnehmer diese Daten in gleicher Struktur, aktuell und gesichert beziehen.

Welchen der Provider in welcher Art und Weise tatsächlich vertraut wird, obliegt der überprüfenden Organisation.

- [Struktur dieses Repositories](./doc/Struktur.md)
- [Ablauf und Methode](./doc/Ablauf.md)
- [Tatsächlicher Trust-Content](./trust-content/)
- [Spezifikation](./spec/)

## ToDo

- [ ] Scripts / Programme für die Methoden schreiben.
- [X] Beispiele
- [ ] Spezifikation vervollständigen und eine CustomResourceDefinition zur Validierung schreiben
- [ ] Automatisationshilfe für das Generieren neuer Fulcio Instanzen im Repo: <https://gitlab.opencode.de/ig-bvc/ag-sig/fulcio-rekor-rollout>
- [ ] Dokumentation vervollständigen und auch im IG-BvC Dokument sowie im Confluence dokumentieren.
